﻿using System;
using fibonacci_library;


namespace fibonacci_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Console.WriteLine($"Args length is: {args.Length}");

            if ( (args.Length == 0) || (args[0] == "-h") || (args[0] == "--help") ) 
            {
                Console.WriteLine("Enter an positional index for the Fibonacci sequence. For example, 10 to see the first 10 elements of the sequence.");
            }

            else if (args.Length == 1)
            {
                if ( Char.IsNumber(args[0].ToString()[0]))
                {
                    int.TryParse(args[0], out int inputVal);
                    GetAndPrintFibSequence(inputVal);

                }

            }  
        }

        static void GetAndPrintFibSequence(int endingIndex)
        {
            FibonacciGenerator gen = new FibonacciGenerator(endingIndex);

            var sequence = gen.GeneratedSequence;
            
            Console.Write($"The first {endingIndex} elements of the Fibonacci sequence are: ");
           
            // using a for loop instead of a foreach because I want to not print a comma after the last element.
            for (int i = 0; i < sequence.Count; i++)
            {
                Console.Write(sequence[i]);
                if (!(i == sequence.Count - 1))
                {
                    Console.Write(", ");
                }
                else Console.WriteLine(".");
            }
        }
    }
}
