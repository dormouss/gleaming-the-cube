using System;
using System.Text;
using System.Collections.Generic;


namespace fibonacci_library
{
    /// <summary>
    /// A class for generating Fibonacci sequences
    /// </summary>
    public class FibonacciGenerator
    {
        /// <summary>
        /// The starting point of the sub-sequence, 0-indexed
        /// </summary>
        private int _subSequenceStartingIndex;

        /// <summary>
        /// The finishing point of the sub-sequence, 0-indexed
        /// </summary>
        private int _subSequenceFinishingIndex;

        // The overall number of elements to generate
        private int _sequenceCount;

        private List<uint> fibSeq;


        /// <summary>
        /// Constructor. Creates a Fibonnaci sequence starting with 0, and up to the index positional value requested by the caller.
        /// </summary>
        /// <param name="startingPoint"></param>
        /// <param name="finishPoint"></param>
        public FibonacciGenerator(int sequenceCount)
        {
            SequenceCount = sequenceCount;
            fibSeq = new List<uint>();
            GenerateSequence();
        }





        private List<uint> GenerateSequence()
        {
            //List<uint> sequence = new List<uint>();
            if (SequenceCount == 0)
            {
                return fibSeq;
            }

            else if (SequenceCount == 1)
            {
                fibSeq.Add(0);
                return fibSeq;
            }
            
            else if (SequenceCount == 2)
            {
                fibSeq.Add(0);
                fibSeq.Add(1);

                return fibSeq;
            }

            else
            {
                fibSeq.Add(0);
                fibSeq.Add(1);

                for (uint i = 2; i < SequenceCount; i++)
                {
                    uint fn1 = fibSeq[(int)i-1];
                    uint fn2 = fibSeq[(int)i-2];
                    fibSeq.Add(fn1+fn2);
                    //Console.WriteLine($"In the loop. \ti: {i} \tSequence value: {fibSeq[(int)i]} \tfn1: {fn1}   \tfn2: {fn2}");
                }
                return fibSeq;
            }
        }

        public List<uint> GeneratedSequence
        {
            get { return fibSeq; }
        }


        public int SequenceCount
        {
            get {return _sequenceCount; }
            set 
            {
                if (value < 0)
                {
                    Console.WriteLine("WARNING: Sequence length cannot be less than 0. Setting starting index to 0.");
                    _sequenceCount = 0;
                }
                else { _sequenceCount = value; }
            }
        } 



        public int SubSequenceStartingValue
        {
            get {return _subSequenceStartingIndex; }
            set 
            {
                if (value < 0)
                {
                    Console.WriteLine("WARNING: Starting index should not be less than 0. Setting starting index to 0.");
                    _subSequenceStartingIndex = 0;
                }
                else if (value > SequenceCount)
                {
                    Console.WriteLine("WARNING: Starting index should not exceed the length of the generated sequence. Setting starting index to sequence length.");
                    _subSequenceStartingIndex = 0;

                }
                else {_subSequenceStartingIndex = value;}
            }
        }

        public int SubSequenceEndingValue
        {
            get {return _subSequenceFinishingIndex; }
            set 
            {
                if (value < 1)
                {
                    Console.WriteLine("WARNING: Sequence count index should not be less than 1. Setting sequence index value to 1.");
                    _subSequenceFinishingIndex = 0;
                }
                else {_subSequenceFinishingIndex = value;}
            }
        }
    }
}