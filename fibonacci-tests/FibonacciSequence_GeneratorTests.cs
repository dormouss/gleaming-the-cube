using System;
using System.Text;
using System.IO;
using System.Collections.Generic;

using Xunit;

using fibonacci_library;

namespace fibonacci_tests
{
    public class Fibonacci_Generator_Tests
    {
        [Fact]
        public void Sequence_Length_Value_Less_Than_Zero_Generates_Warning()
        {
            // Arrange
            TextWriter tmp = Console.Out;   // We're diverting the normal output stream because the Generator constructor will 
                                            // output a message if there's an error. So we capture the current stream so we can
                                            // restore it after the test action (but before the ASSERT).
            
            StringWriter string_writer = new StringWriter();
            Console.SetOut(string_writer);

            string expectedMessage = "WARNING: Sequence length cannot be less than 0. Setting starting index to 0.";

            // Act
            var gen1 = new FibonacciGenerator(-1);
            string actualMessage = string_writer.ToString().TrimEnd(); // TrimEnd() to remove the training '\n'.

            tmp.Flush();
            Console.SetOut(tmp);

            // Assert (expected, actual)
            Assert.Equal(expectedMessage, actualMessage);
        }


        [Fact]
        public void GenerateSequence_With_Index_0_Returns_List_Length_Empty()
        {
            // Arrange
            List<uint> expectedSequence = new List<uint>();

            // Act

            FibonacciGenerator gen = new FibonacciGenerator(0);
            List<uint> generatedSequence = gen.GeneratedSequence;

            // Assert (expected, actual)
            Assert.Equal(expectedSequence, generatedSequence);
        }



        [Fact]
        public void GenerateSequence_With_Index_1_Returns_List_Length_One()
        {
            // Arrange
            List<uint> expectedSequence = new List<uint>{0};

            // Act
            FibonacciGenerator gen = new FibonacciGenerator(1);
            List<uint> generatedSequence = gen.GeneratedSequence;

            // Assert (expected, actual)
            Assert.Equal(expectedSequence, generatedSequence);
        }


        [Fact]
        public void GenerateSequence_With_Index_2_Returns_List_Length_Two()
        {
            // Arrange
            List<uint> expectedSequence = new List<uint>{0, 1};

            // Act
            FibonacciGenerator gen = new FibonacciGenerator(2);
            List<uint> generatedSequence = gen.GeneratedSequence;

            // Assert (expected, actual)
            Assert.Equal(expectedSequence, generatedSequence);
        }


        [Fact]
        public void GenerateSequence_With_StartValue_1_and_EndValue_10_Returns_List_Length_Ten()
        {
            // Arrange
            List<uint> expectedSequence = new List<uint>{0, 1, 1, 2, 3, 5, 8, 13, 21, 34};

            // Act
            FibonacciGenerator gen = new FibonacciGenerator(10);
            List<uint> generatedSequence = gen.GeneratedSequence;

            // Console.WriteLine("Comparing the two lists:");
            // Console.WriteLine($"Expected length is {expectedSequence.Count} and return length is {generatedSequence.Count}");

            // Assert (expected, actual)
            Assert.Equal(expectedSequence, generatedSequence);
        }


        // [Fact]
        // public void Finishing_Value_Less_Than_One_Generates_Warning()
        // {
        //     // Arrange
        //     TextWriter tmp = Console.Out;   // We're diverting the normal output stream because the Generator constructor will 
        //                                     // output a message if there's an error. So we capture the current stream so we can
        //                                     // restore it after the test action (but before the ASSERT).
            
        //     StringWriter string_writer = new StringWriter();
        //     Console.SetOut(string_writer);

        //     string expectedMessage = "WARNING: Sequence count index should not be less than 1. Setting sequence index value to 1.";

        //     // Act
        //     var gen1 = new FibonacciGenerator(1, 0);
        //     string actualMessage = string_writer.ToString().TrimEnd(); // TrimEnd() to remove the training '\n'.

        //     // (and also restore the stream)
        //     tmp.Flush();
        //     Console.SetOut(tmp);

        //     // Assert (expected, actual)
        //     Assert.Equal(expectedMessage, actualMessage);
        // }

    }
}
